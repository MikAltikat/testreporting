package model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import model.entity.ReportSuite;

import java.util.List;

@Repository
public interface ReportSuitesRepository extends CrudRepository<ReportSuite, Integer> {

    List<ReportSuite> findAll();

    List<ReportSuite> findByClassName(String className);

    List<ReportSuite> findByPackageName(String packageName);


}
