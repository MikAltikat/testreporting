package model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import model.entity.Report;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public interface ReportRepository extends CrudRepository<Report, Integer> {

    List<Report> findAll();

    List<Report> findBySuccessTrue();

    List<Report> findBySkippedTrue();

    List<Report> findByErrorTrue();

    List<Report> findByFailedTrue();


    List<Report> findBySuccessFalse();

    List<Report> findByDateBetween(LocalDate d1, Date d2);

    List<Report> findByDateBefore(LocalDate d);

    List<Report> findByDateAfter(LocalDate d);

    List<Report> findByDate(LocalDate d);



    void deleteById(int id);



}
