package model.util;

import org.springframework.stereotype.Component;
import model.entity.Report;
import model.entity.ReportSuite;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class Utils {

    public static double validAverages(List<Report> reportList) {
        double cpt;
        double size = reportList.size();
        cpt = reportList.stream().filter(Report::getValid).count();
        return cpt / size;
    }

    public static double successAverages(List<Report> reportList) {
        double cpt;
        double size = reportList.size();
        cpt = reportList.stream().filter(Report::getSuccess).count();
        return cpt / size;
    }

    public static double failedAverages(List<Report> reportList) {
        double cpt;
        double size = reportList.size();
        cpt = reportList.stream().filter(Report::getFailed).count();
        return cpt / size;
    }

    public static double errorAverages(List<Report> reportList) {
        double cpt;
        double size = reportList.size();
        cpt = reportList.stream().filter(Report::getError).count();
        return cpt / size;
    }

    public static double skippedAverages(List<Report> reportList) {
        double cpt;
        double size = reportList.size();
        cpt = reportList.stream().filter(Report::getSkipped).count();
        return cpt / size;
    }

    public static double durationAverages(List<Report> reportList) {
        double sum;
        double size = reportList.size();
        sum = reportList.stream().mapToDouble(Report::getDuration).sum();
        return sum / size;
    }

    public static Set<Integer> getYears(List<Report> reportList) {
        Set<Integer> years = reportList.stream().map(report -> (report.getDate().getYear())).collect(Collectors.toSet());
        return years;
    }

    public static Set<String> getClassName(ReportSuite reportSuite) {
        Set<String> classNames = reportSuite.getReportList().stream().map(report -> report.getReportSuite().getClassName()).collect(Collectors.toSet());
        return classNames;
    }




}
