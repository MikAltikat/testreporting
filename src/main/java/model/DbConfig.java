package model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties("spring.datasource")
@SuppressWarnings("unused")
public class DbConfig {

    private String driverClassName;
    private String url;
    private String username;
    private String name;

    @Profile("h2")
    @Bean
    public String h2DataBaseConnection(){
        System.out.println("DB Connection to H2");
        System.out.println(driverClassName);
        System.out.println(url);
        return "DB connection to H2";
    }

    @Profile("postgre")
    @Bean
    public String postgreDataBaseConnection(){
        System.out.println("DB Connection to postgresql");
        System.out.println(driverClassName);
        System.out.println(url);
        return "DB connection to postgresql";
    }


}
