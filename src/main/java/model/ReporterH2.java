package model;

import model.entity.Report;
import model.entity.ReportSuite;
import model.manager.DbManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Configuration
@SpringBootApplication
@EnableJpaRepositories(basePackages = {"model"}) //to be able to inject repositories in the context of Spring.

@PropertySource(ignoreResourceNotFound = true, value = "classpath:application-h2.properties")
public class ReporterH2 {

    @Autowired
    DbManager dbManager;

    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(ReporterH2.class);
        builder.headless(false);
        ConfigurableApplicationContext context = builder.run(args);
    }


    @Bean
    public void demo() {
        //       return (args) -> {
//            ReportView reportView = new ReportView(dbManager.getYearSuccessRate());
//            reportView.setVisible(true);

        dbManager.saveSureFireReport("target/surefire-reports");

        List<Report> reportList = new ArrayList<>();

        reportList.add(new Report("myMethode", LocalDate.of(2017, 2, 1), false, false, true, false, "myMessage", 0.15));
        reportList.add(new Report("myMethode", LocalDate.of(2016, 2, 1), true, false, false, false, "myMessage", 0.15));
        reportList.add(new Report("myMethode", LocalDate.of(2015, 2, 1), false, true, false, false, "myMessage", 0.15));
        reportList.add(new Report("myMethode", LocalDate.of(2017, 3, 14), false, false, false, true, "myMessage", 0.15));
        reportList.add(new Report("myMethode", LocalDate.of(2017, 3, 14), true, false, false, false, "myMessage", 0.15));

        dbManager.saveReportSuite(new ReportSuite(reportList, "myClass", "myPackage"));

//        System.out.println(new XMLManager().parseSureFireReport("target/surefire-reports"));

//        System.out.println(dbManager.getSuites());
//        for (Report report : dbManager.getReports()) {
//
//            System.out.println(report.getReportSuite());
//
//
//        }

    }


}



