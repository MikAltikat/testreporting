package model.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import model.manager.DbManager;
import model.manager.XMLManager;
import model.entity.Report;
import model.entity.ReportSuite;
import model.util.Utils;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RestController
public class ReportController {

    @Autowired
    private DbManager dbManager;


    @RequestMapping(value = "getSureFire", produces = APPLICATION_JSON_VALUE)
    public List<ReportSuite> getSureFire() {
        XMLManager xmlManager = new XMLManager();
        return xmlManager.parseSureFireReport("target/surefire-reports");
    }


    @RequestMapping(value = "/getReports", produces = APPLICATION_JSON_VALUE)
    public List<Report> getAllReports() {
        return dbManager.getReports();
    }

    @RequestMapping(value = "/getSuites", produces = APPLICATION_JSON_VALUE)
    public List<ReportSuite> getAllSuites() {
        return dbManager.getSuites();
    }

    @RequestMapping(value = "saveReport", method = RequestMethod.POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public void addReport(HttpEntity<String> httpEntity) {

        String json = httpEntity.getBody();

        dbManager.saveReport(dbManager.convertReportFromJson(json));
    }


    @RequestMapping("/deleteById")
    public void deleteById(@RequestParam(value = "id", required = true) int id) {
        dbManager.deleteReport(id);
    }

    @RequestMapping("/getSuccessReports")
    public List<Report> getSuccessReport() {
        return dbManager.getSuccessReport();
    }

    @RequestMapping("/getNonSuccessReports")
    public List<Report> getNonSuccessReport() {
        return dbManager.getNonSuccessReport();
    }


    @RequestMapping(value = "/getReportAfter", produces = APPLICATION_JSON_VALUE)
    public List<Report> getReportAfter(
            @RequestParam(value = "date", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate time) {
        return dbManager.getReportAfter(time);
    }

    @RequestMapping("/getReportBefore")
    public List<Report> getReportBefore(
            @RequestParam(value = "date", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate time) {
        return dbManager.getReportBefore(time);
    }


    @RequestMapping(value = "/getReportByDate", produces = APPLICATION_JSON_VALUE)
    public List<Report> getReportByDate(
            @RequestParam(value = "date", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate time) {
        return dbManager.getReportByDate(time);
    }

    @RequestMapping("/getReportByYear")
    public List<Report> getReportByYear(
            @RequestParam(value = "year", required = true) int year) {
        return dbManager.getReportByYear(year);
    }

    @RequestMapping("/getYears")
    public Set<Integer> getYears() {
        return Utils.getYears(dbManager.getReports());
    }


    @RequestMapping("/getYearAverage")
    public Map<Integer, Double> getYearAverage() {
        return dbManager.getYearSuccessRate();
    }

    @RequestMapping("/getDateAverage")
    public double getDateAverage(
            @RequestParam(value = "date", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate time) {

        return dbManager.getDateSuccessRate(time);
    }

    @RequestMapping("/getSuitesByClassName")
    public void getSuitesByClassName(@RequestParam(value = "class", required = true) String className) {
        dbManager.getSuiteByClassName(className);
    }

    @RequestMapping("/getSuitesByPackageName")
    public void getSuitesByPackageName(@RequestParam(value = "package", required = true) String packageName) {
        dbManager.getSuiteByClassName(packageName);
    }

}
