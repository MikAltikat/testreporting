package model.manager;

import org.apache.maven.plugins.surefire.report.ReportTestCase;
import org.apache.maven.plugins.surefire.report.ReportTestSuite;
import org.apache.maven.plugins.surefire.report.SurefireReportParser;
import org.apache.maven.reporting.MavenReportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import model.Reporter;
import model.entity.Report;
import model.entity.ReportSuite;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@Component
public class XMLManager {
    private static final Logger log = LoggerFactory.getLogger(Reporter.class);

    public ByteArrayOutputStream marshallList(List<Report> reportList) {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(ReportSuite.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(new ReportSuite(reportList, "", ""), outputStream);

        } catch (JAXBException e) {
            log.error("JAXBException : ", e);
        }
        return outputStream;

    }

    public List<Report> unmarshallList(String path) {
        JAXBContext jaxbContext;
        ReportSuite reportSuite = null;
        try {

            jaxbContext = JAXBContext.newInstance(ReportSuite.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            reportSuite = (ReportSuite) unmarshaller.unmarshal(new File(path));

        } catch (JAXBException e) {
            log.error("JAXBException : ", e);
        }
        return reportSuite.getReportList();
    }

    public void writeXML(ByteArrayOutputStream baos, String path) {
        try (FileOutputStream fos = new FileOutputStream(new File(path))) {
            baos.writeTo(fos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public List<ReportSuite> parseSureFireReport(String path) {

        List<ReportSuite> result = new ArrayList<>();

        SurefireReportParser parser = new SurefireReportParser(
                Arrays.asList(Paths.get(path).toFile())
                , Locale.ENGLISH
                , null);
        List<ReportTestSuite> suites = null;
        try {
            suites = parser.parseXMLReportFiles();
        } catch (MavenReportException e) {
            try {
                throw new IOException("Failed to parse surefire reports", e);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        for (ReportTestSuite rts : suites) {

            result.add(transformReportTestSuiteToReportSuite(rts));
        }
        return result;
    }


    private Report transformReportTestCaseToReport(ReportTestCase reportTestCase) {

        return new Report(reportTestCase.getName()
                , LocalDate.now()
                , reportTestCase.isSuccessful()
                , reportTestCase.hasFailure()
                , reportTestCase.hasSkipped()
                , reportTestCase.hasError()
                , reportTestCase.getFailureDetail()
                , (double) reportTestCase.getTime());

    }

    private ReportSuite transformReportTestSuiteToReportSuite(ReportTestSuite rts) {

        List<Report> list = new ArrayList<>();
        for (ReportTestCase rtc : rts.getTestCases()) {

            list.add(transformReportTestCaseToReport(rtc));
        }
        return new ReportSuite(list, rts.getName(), rts.getPackageName());
    }


//    public static void main(String[] args) {
////        List<Report> reportList = new ArrayList<>();
////
////        reportList.add((new Report("methodeName", LocalDate.of(2010, 10, 10), true, false, false, false, "message", 0.15)));
////        reportList.add((new Report("methodeName", LocalDate.of(2010, 10, 10), true, false, false, false, "message", 0.15)));
////        reportList.add((new Report("methodeName", LocalDate.of(2010, 10, 10), true, false, false, false, "message", 0.15)));
////
//        XMLManager xmlManager = new XMLManager();
////        String path = "C:\\development\\sources\\testReporting\\src\\test\\resources\\reports2.xml";
////        xmlManager.writeXML(xmlManager.marshallList(reportList), path);
////        System.out.println(xmlManager.unmarshallList(path));
//
//
//        //        System.out.println(xmlManager.marshallList(reportList));
//
//        System.out.println(xmlManager.parseSureFireReport("target/surefire-reports"));
//
//
//    }

}
