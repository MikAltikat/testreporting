package model.manager;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.entity.Report;
import model.entity.ReportSuite;
import model.repository.ReportRepository;
import model.repository.ReportSuitesRepository;
import model.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class DbManager {

    @Autowired
    private model.repository.ReportRepository reportRepository;
    @Autowired
    private ReportSuitesRepository suiteRepository;

    private DbManager() {
    }

    public DbManager(ReportRepository reportRepository) {
        this.reportRepository = reportRepository;
    }


    private final String XMLPATH = "src\\main\\resources\\reports.xml";

    public void saveReport(Report report) {
        reportRepository.save(report);
    }

    public void saveSureFireReport(String path) {

//        suiteRepository.saveAll(setIds(new XMLManager().parseSureFireReport(path)));

        for (ReportSuite reportSuite : new XMLManager().parseSureFireReport(path)) {
            saveReportSuite(reportSuite);
        }
    }

    public void saveReportSuite(ReportSuite reportSuite) {
        List<ReportSuite> suites = new ArrayList<>();
        suiteRepository.save(reportSuite);
        suites.add(reportSuite);
        setIds(suites);
        reportRepository.saveAll(reportSuite.reportList);
    }

    public void saveAll(ReportSuite reportSuite) {
        reportRepository.saveAll(reportSuite.getReportList());
    }

    public List<Report> getReports() {
        return reportRepository.findAll();
    }

    public List<Report> getSuccessReport() {
        return reportRepository.findBySuccessTrue();
    }

    public List<Report> getNonSuccessReport() {
        return reportRepository.findBySuccessFalse();
    }

    public List<Report> getReportBefore(LocalDate date) {
        return reportRepository.findByDateBefore(date);
    }

    public List<Report> getReportAfter(LocalDate date) {
        return reportRepository.findByDateAfter(date);
    }

    public List<Report> getFailedReports() {
        return reportRepository.findByFailedTrue();
    }

    public List<Report> getSkippedReports() {
        return reportRepository.findBySkippedTrue();
    }

    public List<Report> getErrorReports() {
        return reportRepository.findByErrorTrue();
    }


    public void saveFromXML() {
        XMLManager XMLManager = new XMLManager();
        reportRepository.saveAll(XMLManager.unmarshallList(XMLPATH));
    }

    public List<Report> getReportByYear(int year) {
        List<Report> result = getReports().stream().filter(report -> report.getDate().getYear() == year)
                .collect(Collectors.toList());

        return result;
    }

    public List<Report> getReportByMonth(int month) {

        List<Report> result = getReports().stream()
                .filter(report -> report.getDate().getMonthValue() == month)
                .collect(Collectors.toList());

        return result;
    }

    public List<Report> getReportByDate(LocalDate date) {
        return reportRepository.findByDate(date);
    }

    public void deleteReport(int id) {
        reportRepository.deleteById(id);
    }

    public void deleteAll() {
        reportRepository.deleteAll();
        suiteRepository.deleteAll();
    }

    public Map<Integer, Double> getYearSuccessRate() {

        Map<Integer, Double> successRates = new HashMap<>();
        List<Report> result;
        double sum;

        for (Integer year : Utils.getYears(reportRepository.findAll())) {
            result = getReportByYear(year);
            sum = 0;
            sum += result.stream().filter(Report::getSuccess).count();
            successRates.put(year, sum / result.size());
        }
        return successRates;
    }

    public Double getDateSuccessRate(LocalDate date) {

        List<Report> reportOfTheDay = getReportByDate(date);
        double cpt = reportOfTheDay.stream().filter(Report::getSuccess).count();
        double size = reportOfTheDay.size();
        return cpt / size;
    }

    //// Methode to move.
    public Report convertReportFromJson(String jsonReport) {
        ObjectMapper objectMapper = new ObjectMapper();
        Reader reader = new StringReader(jsonReport);
        Report report = null;
        try {
            report = objectMapper.readValue(reader, Report.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return report;
    }

    public List<ReportSuite> getSuites() {
        return suiteRepository.findAll();
    }

    public List<ReportSuite> getSuiteByClassName(String name) {
        return suiteRepository.findByClassName(name);
    }

    private List<ReportSuite> setIds(List<ReportSuite> reportSuites) {
        for (ReportSuite reportSuite : reportSuites) {
            for (Report report : reportSuite.reportList) {
                report.setReportSuite(reportSuite);
            }
        }
        return reportSuites;
    }

}
