package model.entity;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import model.adapter.LocalDateAdapter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@XmlRootElement(name = "reports")
@XmlAccessorType(XmlAccessType.FIELD)
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //strat identity to avoid hibernate_sequence error
    @XmlElement
    private Integer id;
    @XmlElement
    @Column(name="METHOD_NAME")
    private String methodName;
    @XmlElement
    @Column(name="MESSAGE")
    private String message;
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    @XmlElement
    @Column(name = "EXECUTION_DATE")
    private LocalDate date;
    @XmlElement
    @Column(name = "SUCCESS")
    private boolean success;
    @XmlElement
    @Column(name = "ERROR")
    private boolean error;
    @XmlElement
    @Column(name = "SKIPPED")
    private boolean skipped;
    @XmlElement
    @Column(name = "FAILED")
    private boolean failed;
    @XmlElement
    @Column(name = "DURATION", precision = 5, scale = 5)
    private double duration;
    @Transient
    @XmlElement
    private boolean valid;

    @ManyToOne
    @JoinColumn(name = "report_suite_id")
//    @JsonIgnore
    @XmlElement
    private ReportSuite reportSuite;


    public Report() {
    }

    public Report(String methodName, LocalDate date, Boolean success,
                  Boolean failed, Boolean skipped, Boolean error, String message, Double duration) {


        this.error = error;
        this.skipped = skipped;
        this.success = success;
        this.date = date;
        this.duration = duration;
        this.failed = failed;
        this.methodName = methodName;
        this.message = message;

    }

    private boolean isValid() {
        int cpt = 0;
        List<Boolean> flags = new ArrayList<>();
        flags.add(failed);
        flags.add(success);
        flags.add(skipped);
        flags.add(error);

        for (boolean state : flags) {
            if (state) {
                cpt++;
            }
        }
        return cpt == 1;

    }

    public ReportSuite getReportSuite() {
        return reportSuite;
    }

    public void setReportSuite(ReportSuite reportSuite) {
        this.reportSuite = reportSuite;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methode_name) {
        this.methodName = methode_name;
    }


    public String getMessage() {
        return message;
    }


    @JsonGetter
    public boolean getValid() {
        this.valid = isValid();
        return this.valid;
    }


    public boolean getFailed() {
        return failed;
    }


    public boolean getError() {
        return error;
    }


    public boolean getSkipped() {
        return skipped;
    }


    public double getDuration() {
        return duration;
    }


    public Integer getId() {
        return id;
    }


    public Boolean getSuccess() {
        return success;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return ("Report{" +
                "id=" + id +
                ", methode_name='" + methodName + '\'' +
                ", message='" + message + '\'' +
                ", date=" + date +
                ", success=" + success +
                ", error=" + error +
                ", skipped=" + skipped +
                ", failed=" + failed +
                ", duration=" + duration +
                ", valid=" + valid +
                ", report_suite=" + reportSuite +
                '}');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Report report = (Report) o;
        return error == report.error &&
                skipped == report.skipped &&
                failed == report.failed &&
                Double.compare(report.duration, duration) == 0 &&
                valid == report.valid &&
                Objects.equals(id, report.id) &&
                Objects.equals(methodName, report.methodName) &&
                Objects.equals(message, report.message) &&
                Objects.equals(date, report.date) &&
                Objects.equals(success, report.success) &&
                Objects.equals(reportSuite, report.reportSuite);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, methodName, message, date, success, error, skipped, failed, duration, valid, reportSuite);
    }
}
