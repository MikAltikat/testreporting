package model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ReportSuite")
@XmlSeeAlso({Report.class})

@Entity
@JsonPropertyOrder({"packageName", "className", "reportList"})
public class ReportSuite {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //strat identity to avoid hibernate_sequence error
    private Integer id;

    @XmlElementWrapper(name = "reports")
    @XmlElement(name = "report")
    @JsonIgnore
    @OneToMany(mappedBy = "reportSuite", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public List<Report> reportList;

    @XmlElement
    @JsonProperty
    @Column(name = "class_name")
    private String className;

    @XmlElement
    @JsonProperty
    @Column(name = "package_name")
    private String packageName;

    public ReportSuite() {

    }

    public ReportSuite(List<Report> reportList, String className, String packageName) {
        this.reportList = reportList;
        this.className = className;
        this.packageName = packageName;
    }


    public List<Report> getReportList() {
        return reportList;
    }

    public String getClassName() {
        return className;
    }

    public String getPackageName() {
        return packageName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportSuite that = (ReportSuite) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(reportList, that.reportList) &&
                Objects.equals(className, that.className) &&
                Objects.equals(packageName, that.packageName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, reportList, className, packageName);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ReportSuite{" +
                "id=" + id +
                ", class_name='" + className + '\'' +
                ", package_name='" + packageName + '\'' +
                ", totalDuration='" + getTotalDuration() + '\'' +
                '}');
        return sb.toString();
    }

    @JsonIgnore
    public int getNumberOfSuccess() {
        return (int) reportList.stream()
                .filter(Report::getSuccess)
                .count();
    }

    @JsonIgnore
    public int getNumberOfFailed() {
        return (int) reportList.stream()
                .filter(Report::getFailed)
                .count();
    }

    @JsonIgnore
    public int getNumberOfSkipped() {
        return (int) reportList.stream()
                .filter(Report::getSkipped)
                .count();
    }

    @JsonIgnore
    public int getNumberOfError() {
        return (int) reportList.stream().filter(Report::getError).count();
    }

    public Double getTotalDuration() {
        return reportList.stream()
                .mapToDouble(Report::getDuration)
                .sum();
    }

    @JsonIgnore
    public Boolean isValid() {
        return reportList.stream().
                allMatch(Report::getValid);
    }

    public void addReport(Report report) {
        if (reportList == null) {
            reportList = new ArrayList<>();
        }

        report.setReportSuite(this);
        reportList.add(report);
    }


}
