package view;

import model.entity.Report;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.Map;

@Component
public class ReportView extends JFrame {


    public ReportView(List<Report> reports) {

        int val;

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
                System.exit(0);
            }
        });


        JPanel pnl = new JPanel(new BorderLayout());
        setContentPane(pnl);
        setSize(800, 500);

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();


        for (Report report : reports) {

            if (report.getSuccess()) {
                val = 10;
            } else val = 1;
            dataset.addValue(val,
                    report.getReportSuite().getClassName(),
                    Integer.toString(report.getDate().getYear()));

        }


        JFreeChart barChart = ChartFactory.createBarChart("Evolution des Report", "",
                "Resultat des Report", dataset, PlotOrientation.VERTICAL, false, true, false);
        ChartPanel cPanel = new ChartPanel(barChart);
        pnl.add(cPanel);
    }


    public ReportView(Map<Integer, Double> averageMap) {

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
                System.exit(0);
            }
        });

        JPanel pnl = new JPanel(new BorderLayout());
        setContentPane(pnl);
        setSize(800, 500);

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (Map.Entry<Integer, Double> entry : averageMap.entrySet()) {
            dataset.addValue(entry.getValue(), "", entry.getKey());
        }

        JFreeChart barChart = ChartFactory.createBarChart("Report Averages by year", "Years",
                "Report Averages", dataset, PlotOrientation.VERTICAL, false, true, false);
        ChartPanel cPanel = new ChartPanel(barChart);
        pnl.add(cPanel);
    }
}