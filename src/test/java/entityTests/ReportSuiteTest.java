package entityTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import model.entity.Report;
import model.entity.ReportSuite;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ReportSuiteTest {

    ReportSuite reportSuite;

    @BeforeEach
    public void setupObjectToTest() {

        List<Report> reportList = new ArrayList<>();
        Report report1 = (new Report("myMethode", LocalDate.of(2017, 2, 1), false, false, true, false, "myMessage", 0.15));
        Report report2 = (new Report("myMethode", LocalDate.of(2016, 2, 1), true, false, false, false, "myMessage", 0.15));
        Report report3 = (new Report("myMethode", LocalDate.of(2015, 2, 1), false, true, false, false, "myMessage", 0.15));
        Report report4 = (new Report("myMethode", LocalDate.of(2017, 3, 14), false, false, false, true, "myMessage", 0.15));
        Report report5 = (new Report("myMethode", LocalDate.of(2017, 3, 14), true, false, false, false, "myMessage", 0.15));


        reportList.add(report1);
        reportList.add(report2);
        reportList.add(report3);
        reportList.add(report4);
        reportList.add(report5);


        reportSuite = new ReportSuite(reportList, "className", "packageName");
    }


    @Test
    void getNumberOfSuccess() {
        assertEquals(reportSuite.getNumberOfSuccess(), 2);
    }

    @Test
    void getNumberOfFailed() {
        assertEquals(reportSuite.getNumberOfFailed(), 1);
    }

    @Test
    void getNumberOfError() {
        assertEquals(reportSuite.getNumberOfError(), 1);
    }

    @Test
    void getNumberOfSkipped() {
        assertEquals(reportSuite.getNumberOfSkipped(), 1);
    }

    @Test
    void getTotalDurationTest() {
        assertEquals(reportSuite.getTotalDuration(), 0.75);
    }

    @Test
    void isValidTest() {
        assertTrue(reportSuite.isValid());
    }


}
