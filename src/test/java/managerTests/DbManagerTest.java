package managerTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import model.manager.DbManager;
import model.entity.Report;
import model.repository.ReportRepository;
import model.entity.ReportSuite;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DbManagerTest {

    @Mock
    ReportRepository reportRepository;
    ReportSuite reportSuite;

    DbManager dbManager;
    Report report1;
    Report report2;
    Report report3;
    Report report4;
    Report report5;
    List<Report> reportList = new ArrayList<>();
    List<Report> resultList;
    List<Report> successList = new ArrayList<>();


    @BeforeEach
    void setupObjectToTest() {

        dbManager = new DbManager(reportRepository);
        report1 = (new Report("myMethode", LocalDate.of(2017, 2, 1), false, false, true, false, "myMessage", 0.15));
        report2 = (new Report("myMethode", LocalDate.of(2016, 2, 1), true, false, false, false, "myMessage", 0.15));
        report3 = (new Report("myMethode", LocalDate.of(2015, 2, 1), false, true, false, false, "myMessage", 0.15));
        report4 = (new Report("myMethode", LocalDate.of(2017, 3, 14), false, false, false, true, "myMessage", 0.15));
        report5 = (new Report("myMethode", LocalDate.of(2017, 3, 14), true, false, false, false, "myMessage", 0.15));

        reportList.add(report1);
        reportList.add(report2);
        reportList.add(report3);
        reportList.add(report4);
        reportList.add(report5);

        successList.add(new Report("myMethode", LocalDate.of(2016, 2, 1), true, false, false, false, "myMessage", 0.15));
        successList.add((new Report("myMethode", LocalDate.of(2017, 3, 14), true, false, false, false, "myMessage", 0.15)));


        reportSuite = new ReportSuite(reportList, "className", "packageName");

    }

    @Test
    void testGetReport() {
        when(reportRepository.findAll()).thenReturn(reportList);
        assertEquals(dbManager.getReports().size(), 5);
    }

    @Test
    void getReportByYearTest() {
        when(reportRepository.findAll()).thenReturn(reportList);

        resultList = dbManager.getReportByYear(2017);

        assertTrue(resultList.size() > 0);
        for (Report report : resultList) {
            assertTrue(report.getDate().getYear() == 2017);
        }
    }

    @Test
    void getReportByMonthTest() {

        when(reportRepository.findAll()).thenReturn(reportList);
        resultList = dbManager.getReportByMonth(2);

        assertTrue(resultList.size() > 0);
        for (Report report : resultList) {

            assertTrue(report.getDate().getMonthValue() == 2);

        }
    }


    @Test
    void getYearSuccessRateTest() {

        when(reportRepository.findAll()).thenReturn(reportList);
        Map<Integer, Double> resultMap = dbManager.getYearSuccessRate();

        double result1 = resultMap.get(2017);
        double result2 = resultMap.get(2015);

        assertEquals(1.0 / 3.0, result1);
        assertEquals(0.0, result2);
    }


    @Test
    void getDateSuccesRateTest() {

        List<Report> listByDate = new ArrayList<>();
        listByDate.add(report4);
        listByDate.add(report5);
        doReturn(listByDate)
                .when(reportRepository).findByDate(LocalDate.of(2017, 3, 14));

        double result = dbManager.getDateSuccessRate(LocalDate.of(2017, 3, 14));
        assertEquals(0.5, result);

    }

    @Test
    void getSuccessReportTest() {

        List<Report> list = reportList.stream()
                .filter(Report::getSuccess)
                .collect(Collectors.toList());

        doReturn(list)
                .when(reportRepository).findBySuccessTrue();

        resultList = dbManager.getSuccessReport();

        for (Report report : resultList) {
            assertTrue(report.getSuccess());
        }
    }


    @Test
    void getNonSuccessReportTest() {

        List<Report> list = new ArrayList<>();
        for (Report report : reportList) {
            if (!report.getSuccess()) {
                list.add(report);
            }
        }

        doReturn(list)
                .when(reportRepository).findBySuccessTrue();

        resultList = dbManager.getSuccessReport();

        for (Report report : resultList) {
            assertFalse(report.getSuccess());
        }
    }


    @Test
    void convertReportFromJsonTest() {
        String jsonReport = "{\"id\":1,\"success\":true,\"date\":\"2010-10-10\",\"error\":false,\"skipped\":false,\"failed\":false,\"duration\":0.15}";
        Report report = dbManager.convertReportFromJson(jsonReport);

        assertNotNull(report);
    }


}
