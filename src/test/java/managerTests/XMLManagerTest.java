package managerTests;

import model.entity.Report;
import model.manager.XMLManager;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;


public class XMLManagerTest {

    @Test
    void marshalListTest() throws IOException {

        List<Report> reportList = new ArrayList<>();

        reportList.add((new Report("methodName", LocalDate.of(2010, 10, 10), true, false, false, false, "message", 0.15)));
        reportList.add((new Report("methodName", LocalDate.of(2010, 10, 10), true, false, false, false, "message", 0.15)));
        reportList.add((new Report("methodName", LocalDate.of(2010, 10, 10), true, false, false, false, "message", 0.15)));


        XMLManager xmlManager = new XMLManager();
        byte[] bytes = xmlManager.marshallList(reportList).toByteArray();

//        InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream("managerTests/file.xml");

        String expected = IOUtils.toString(this.getClass().getResourceAsStream("file.xml"), StandardCharsets.UTF_8);
        assertEquals(expected.trim(), new String(bytes).trim());
    }

    @Test
    void unmarshallTest() {
        XMLManager xmlManager = new XMLManager();
        List<Report> reportList = new ArrayList<>();

        reportList.add(new Report("methodName", LocalDate.of(2010, 10, 10), true, false, false, false, "message", 0.15));
        reportList.add(new Report("methodName", LocalDate.of(2010, 10, 10), true, false, false, false, "message", 0.15));
        reportList.add(new Report("methodName", LocalDate.of(2010, 10, 10), true, false, false, false, "message", 0.15));

        assertIterableEquals(reportList, xmlManager.unmarshallList("src/test/resources/managerTests/file.xml"));

    }
}
