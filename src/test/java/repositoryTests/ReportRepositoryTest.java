package repositoryTests;

import configuration.SpringTestApplication;
import model.entity.Report;
import model.repository.ReportRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {SpringTestApplication.class})

public class ReportRepositoryTest {

    @Autowired
    private ReportRepository reportRepository;

    @Test
    void givenReportRepository_whenSaveAndRetreiveEntity_thenOK() {
        reportRepository.save(new Report("methodeName", LocalDate.of(2000, 10, 10), true, false, false, false, "message", 0.15));

        System.out.println(reportRepository.findAll());
        assertEquals(reportRepository.findAll().size(),1);
    }

}
