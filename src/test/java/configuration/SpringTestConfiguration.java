package configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = {"model"})
//@EnableTransactionManagement
//@EnableAutoConfiguration
public class SpringTestConfiguration {


}
