package utilTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import model.entity.Report;
import model.util.Utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UtilsTest {
    Report report1;
    Report report2;
    Report report3;
    Report report4;
    Report report5;
    List<Report> reportList = new ArrayList<>();

    @BeforeEach
    void setupObjectToTest() {

        report1 = (new Report("myMethod", LocalDate.of(2017, 2, 1), false, false, true, false, "myMessage", 0.15));
        report2 = (new Report("myMethod", LocalDate.of(2016, 2, 1), true, false, false, false, "myMessage", 0.15));
        report3 = (new Report("myMethod", LocalDate.of(2015, 2, 1), false, true, false, false, "myMessage", 0.15));
        report4 = (new Report("myMethod", LocalDate.of(2017, 3, 14), false, false, false, true, "myMessage", 0.15));
        report5 = (new Report("myMethod", LocalDate.of(2017, 3, 14), true, false, false, false, "myMessage", 0.15));


        reportList.add(report1);
        reportList.add(report2);
        reportList.add(report3);
        reportList.add(report4);
        reportList.add(report5);
    }


    @Test
    void successAverageTest() {
        assertEquals(Utils.successAverages(reportList), 0.4);
    }

    @Test
    void errorAverageTest() {
        assertEquals(Utils.errorAverages(reportList), 0.2);
    }

    @Test
    void failedAverageTest() {
        assertEquals(Utils.failedAverages(reportList), 0.2);
    }

    @Test
    void skippedAverageTest() {
        assertEquals(Utils.skippedAverages(reportList), 0.2);
    }

    @Test
    void durationAverageTest() {
        assertEquals(Utils.durationAverages(reportList), 0.15);
    }

    @Test
    void getYearsTest() {

        Set<Integer> resultSet;
        resultSet = Utils.getYears(reportList);
        List<Integer> years = new ArrayList<>();
        years.add(2015);
        years.add(2017);
        years.add(2016);

        assertEquals(resultSet.size(), 3);
        assertTrue(resultSet.containsAll(years));
    }
}
